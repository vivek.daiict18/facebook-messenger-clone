import firebase from 'firebase'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCSwJMKR0JEQ4aIau71VgV2SJoYYb5FnNY",
    authDomain: "facebook-messenger-clone-773d8.firebaseapp.com",
    databaseURL: "https://facebook-messenger-clone-773d8.firebaseio.com",
    projectId: "facebook-messenger-clone-773d8",
    storageBucket: "facebook-messenger-clone-773d8.appspot.com",
    messagingSenderId: "501880898499",
    appId: "1:501880898499:web:46aa8706afbfc4981a3376"
});

const db = firebaseApp.firestore()

export default db