import React from 'react'
import { Card, CardContent, Typography } from '@material-ui/core'
import "./Message.css"

function Message({ message, username }) {
    const isUser = username === message.username;
    return (
        <Card className={`message__card ${isUser && 'ml-auto'}`}>
            <CardContent className="message__cardContent">
                <Typography className="message_typography">
                    {!isUser && `${message.username || 'Unknown'}: `}{message.message}
                </Typography>
            </CardContent>
        </Card>
    )
}

export default Message
