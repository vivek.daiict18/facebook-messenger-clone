import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import { FormControl } from '@material-ui/core'
import Message from './Message';
import db from './firebase';
import * as firebase from 'firebase'
import SendIcon from '@material-ui/icons/Send';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

function App() {
  const [input, setInput] = useState('');
  const [messages, setMessages] = useState([])
  const [username, setUsername] = useState('');

  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(scrollToBottom, [messages]);

  useEffect(() => {
    setUsername(prompt("Enter username"));
  }, [])

  useEffect(() => {
    db.collection('messages')
    .orderBy('timestamp', 'asc')  
    .onSnapshot(snapshot => {
      setMessages(snapshot.docs.map(doc => ({id: doc.id, data: doc.data()})))
    })
  }, [])

  const sendMessage = (event) => {
    event.preventDefault();

    db.collection('messages').add({
      message: input,
      username: username,
      timestamp: firebase.firestore.FieldValue.serverTimestamp()
    })

    setInput('')
  }

  return (
    <div className="App">
      <span className="app__welcomeContent"><AccountCircleIcon className="app__accountIcon" /> {username}</span>

      <div className="app__messages">
        {messages.map(message =>
          (
            <Message key={message.id} username={username} message={message.data} />
          )
        )}
        <div ref={messagesEndRef} />
      </div>

      <form className='app__form'>
        <FormControl className="app__formControl">
          <input
            label="Enter a message..." 
            value={input} 
            onChange={(event) => setInput(event.target.value)} 
            className="app__textField"
          />
          <button disabled={!input} className="app__sendButton" type="submit" onClick={sendMessage}>
          <SendIcon className="app__sendIcon" />
          </button>

        </FormControl>
      </form>
    </div>
  );
}

export default App;
